use_bpm 60 # 128

r=1
b=0.5
bp=0.75
n=0.25
np=0.375
c=0.125
cp=0.1875
sc=0.0625
scp=0.09375
f=0.03125
sf=0.015625

in_thread do
  loop do
    cue :tick
    sleep 0.5
  end
end

1.times do
  sync :tick
  puts "slow drums"
  sample :drum_heavy_kick, rate: 0.8
  # sleep 0.5
end

in_thread do
  60.times do
    sync :tick
    puts "slow drums"
    sample :drum_heavy_kick, rate: 0.8
    sleep 0.5
    sample "/home/vate01/proyectos/sonic-pi", "clap"
    # sleep 0.5
  end
end

in_thread do
  sleep 10
  50.times do
    puts "hit-hat"
    sync :tick
    sample :drum_cymbal_closed#, amp: 0.3
    sleep 0.25
    sample :drum_cymbal_pedal#, amp: 0.3
    # sleep 0.25
  end
end

use_synth :tb303
sleep 5
use_synth_defaults release: 0.125, cutoff: 50, res: 0.8, wave: 0 #, amp: 0.3
in_thread do
  5.times do
    puts "bass"
    #sync :tick
    play_pattern_timed [:gb1, :gb1, :gb1,
                        :gb2, :gb2,
                        :bb2, :bb2, :bb2, :bb2, :bb2, :bb2,
                        :eb1, :eb1, :eb1,
                        :b1, :r, :b1, :b1, :b1, :b1,
                        :bb1,:bb1
    ],
      [n,n,n,n,
       n,n,n,n,
       n,c,c,n,n,
       n,n,n,c,c,
       n,n,n,n
       ]
      end
end

use_synth :piano
sleep 5
in_thread do
  1.times do
    puts "first lead"
    #sync :tick
    play_pattern_timed [:bb3, :gb4, :gb4, :f4, # 1
                        :r, :f4, :f4, :eb4, # 2
                        :r, :eb4, :db4, :eb4, # 3
                        :eb4, :eb4, :f4, :eb4, # 4
                        :db4, :bb3, :r, :bb3, # 5
                        :gb4, :gb4, :f4, :r, # 6
                        :f4, :f4, :eb4, :r, # 7
                        :eb4, :db4, :eb4, :f4,
                        :r
    ],
    [
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      n,n,n,n,
      2
    ]
  end
end

use_synth :pulse
use_synth_defaults release: 0.5, cutoff: 50, res: 0.8, wave: 0# , amp: 0.3
in_thread do
  sleep 10
  1.times do
    puts "second lead"
    #sync :tick
    with_octave 1 do
      play_pattern_timed [:bb4, :bb4, :gb5, :f5,
                          :r, :f5, :f5, :gb5,
                          :eb5, :r, :eb5, :eb5,
                          :db5, :eb5, :f5, :eb5,
                          :db5, :eb5, :db5, :bb4,
                          :r, :bb4, :b4, :bb4,
                          :gb5, :f5, :r, :f5,
                          :gb5, :f5, :gb5, :eb5,
                          :r, :eb5, :db5, :eb5,
                          :db5, :eb5, :gb5, :eb5
      ],
      [
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n,
        n, n, n, n
      ]
    end
  end
end