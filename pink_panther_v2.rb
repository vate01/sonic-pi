# Welcome to Sonic Pi v3.1
# use_synth :piano
use_bpm 47
# repositorio de donde tomÃ© las funciones
# https://gist.github.com/rbnpi/01008ecad37ecb18e9a0
# o
# https://gist.github.com/nppoly/02aa3169029a114891ac3d121bb8650e
# ideas en:
# https://in-thread.sonic-pi.net/t/adding-an-orchestra-to-sonic-pi-using-samples/141
# y mÃ¡s complejo en
# https://rbnrpi.wordpress.com/2016/03/16/sonatina-symphonic-orchestra-revisited-to-give-55-sample-voices-for-sonic-pi/

muestras0 = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Harp/"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Flutes/"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Basses/"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Tuba/"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Violas"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Cello/"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Celli"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Clarinets"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Violin 2"
##| muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Trumpets/"
muestras = "/home/vate01/Descargas/sonic_pi_related/GrandPiano/GrandPiano"

muestra = muestras, "p_c4"
print muestra
print muestras
#sample muestra
##| define :pl do |inst,samplepitch,nv,dv,vol=1,damp=1|
##|   shift=note(nv)-note(samplepitch)
##|   if damp==1 then
##|     sample inst,rate: (pitch_to_ratio shift), sustain: 0.7*dv, release: 0.1*dv, amp: vol
##|   else
##|     sample inst,rate: (pitch_to_ratio shift),sustain: 0.7*dv,release: dv,amp: vol #no damping
##|   end
##| end

##| define :plarray do |samplepitch,narray,darray,vol=1,shift=0|
##|   narray.zip(darray) do |nv,dv|
##|     if nv != :r
##|       pl([muestras, "p_c4"], samplepitch,note(nv)+shift,dv,vol)
##|     end
##|     sleep dv
##|   end
##| end


#this function plays the sample at the relevant pitch for the note desired
#the note duration is used to set up the envelope parameters
define :pl do |inst,samplepitch,nv,dv,vol=1,damp=1|
  shift=note(nv)-note(samplepitch)
  if damp==1 then
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: 0.2*dv,amp: vol
  else
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: dv,amp: vol #no damping
  end
end

# pl([muestras, "p_c4"], :c4, :d4, 1)

#this function plays an array of notes and associated array of durations
#also uses sample name (inst), sample normal pitch, and shift (transpose) parameters
define :plarray do |inst,samplepitch,narray,darray,vol=1,shift=0|
  narray.zip(darray) do |nv,dv|
    if nv != :r
      pl(inst,samplepitch,note(nv)+shift,dv,vol)
    end
    sleep dv
  end
end

# play muestra
# plarray(:c4, cf, tcf)
# plarray([muestras, "p_c4"], :c4,[:c4, :d4, :e4, :c4], [1, 1, 1, 1])

# plarray(muestra, ":c4", ":a4", [:c4, :d4, :e4, :c4], [1, 1, 1, 1])

#put the sample names and pitches into an array i
# i=[[inst0,samplepitch0],[inst1,samplepitch1],[inst2,samplepitch2]]


##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.1, 1, 1, 1])
##| sleep 1
##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.25, 0.25, 0.25, 0.25])

# plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
# plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
# plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])
# plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])

# plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])
# plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])

# plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1])
# plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1]


r=1
b=0.5
bp=0.75
n=0.25
np=0.375
c=0.125
cp=0.1875
sc=0.0625
scp=0.09375
f=0.03125
sf=0.015625

# 1
cs=[:gs3, :a3, :as3]
tcs=[sc, cp, sc]

# cs +=[:b3]
# tcs+=[1]

cs +=[:b3, :gs3, :a3, :as3]
tcs +=[1.875, scp, cp, sc]

# cs +=[:b3]
# tcs+=[1]

# 2
cf=[:c2, :d2, :ds2]
tcf=[sc, cp, sc]

# cf+=[:e2]
# tcf+=[1]

cf+=[:e2, :cs2, :e2, :es2]
tcf+=[1.875, sc, cp, sc]

# cf+=[:e2]
# tcf+=[r]

#  3
cs +=[:b3, :bf3, :a3, :af3]
tcs+=[1.875, sc, cp, sc]

cs +=[:g3, :gs3, :a3, :af3]
tcs+=[1.875, sc, cp, sc]

# 4
cf+=[:e2, :ef2, :d2, :df2]
tcf+=[1.875, sc, cp, scp]

cf+=[:c2, :cs2, :d2, :ds2]
tcf+=[1.875, sc, cp, sc]



# 5
cs += [:b3, :r, :ds4]
tcs +=[1.75, cp, sc]

cs +=  [:e4, :r, :f4, :g4, :r, :ds4]
# cs += [:g2, :r, :a2, :b2, :r, :fs2]
tcs +=[n, cp, sc, n, cp, sc]

# 6
cf += [:e2, [:cs2, :gs2], [:d2, :a2], [:ds2, :as2]]
tcf+=[1.875, sc, cp, sc]

cf+=[[:b2, :e2], :r, :b2, :r]
tcf+=[n, n, n, n]



# 7
cs +=[:e4, :f4, :g4, :c5, :b4, :e4, :g4, :b4]
tcs +=[cp, scp, cp, sc, cp, sc, cp, sc]

cs +=[[:bf4,:e4], :a4, :g4, :e4, :d4]
tcs += [0.75, c, c, c, 0.75]

cs +=[:e4, :r, :r, :ds4]
tcs+=[b, n, cp, sc]



# 8
cf +=[:e2, :r, :b2, [:bs2, :es2], [:d2, :a2], [:ds2, :as2]]
tcf+=[n, n, cp, sc, cp, sc]

cf += [[:g2, :c2], :r, :g2, :r]
tcf +=[n, n, n, n]

cf += [:c2, :r, :g2, [:gs2, :cs2], [:d2, :a2],[:d2, :a2]]
tcf +=[n, n, cp, sc, cp, sc]

# 9
cs+=[:e4, :r, :f4, :g4, :r, :ds4]
tcs +=[n, cp, sc, n, cp, sc]

cs +=[:e4, :f4, :g4, :c5, :b4, :g4, :b4, :e5]
tcs +=[cp, sc, cp, sc, cp, sc, cp, sc]

# 10
cf +=[[:e2, :b2], :r, :b2, :r]
tcf+=[n, n, n, n]

cf += [[:e2, :b2], :r, :b2, [:d2, :a2], [:ds2, :as2], [:e2, :b2]]
tcf +=[n, n, cp, sc, cp, sc]

cf += [[:f2, :c3], :r, :c2, :r]
tcf +=[n, n, n, n]

# 11
cs +=[[:ef5, :a4], :r, :ds4]
tcs += [1.75, cp, sc]

cs +=[:e4, :r, :f4, :g4, :r, :ds4]
tcs += [n, cp, sc, n, cp, sc]

cs +=[:e4, :f4, :g4, :c5, :b4, :e4, :g4, :b4]
tcs +=[cp, sc, cp, sc, cp, sc, cp, sc]

# 12
cf +=[[:f2, :c3], :r, :c2, [:cs2, :gs4], [:d2, :a2], [:ds2, :as2]]
tcf +=[n, n, cp, sc, cp, sc]

cf +=[[:e2, :b2], :r, :b2, :r]
tcf +=[n, n, n, n]

cf += [:e2, :r, :b2, [:bf2, :ef2], [:d2, :a2], [:as2, :ds2]]
tcf +=[n, n, cp, sc, cp, sc]

# 13
cs +=[[:e4, :bf4], :a4, :g4, :e4, :d4, :e4, :r]
tcs +=[b, c, c, c, c, 0.5, 0.5]

cs += [:r, :e5, :d5, :b4, :a4, :g4, :e4]
tcs += [n, cp, sc, cp, sc, cp, sc]

# 14
cf +=[[:g2, :c2], :r, :g2, :r]
tcf+=[n, n, n, n]

cf +=[:c2, :r, :g2, [:cs2, :gs2], [:d2, :a2], [:ds2, :as2]]
tcf +=[n, n, cp, sc, cp, sc]

cf += [[:e2, :b2], :r, :r]
tcf += [n, n, b]

# 15
cs +=[:bf4, :a4, :bf4, :a4, :bf4, :a4, :bf4, :a4]
tcs +=[sc, cp, sc, cp, sc, cp, sc, cp]

# ojo con esta parte:
cs +=[[:g4,:b3], :e4, :d4, :e4, :e4]
tcs +=[c, c, c, c, b]

cs +=[[:e4, :b3], [:b3, :g4], :r, [:b3, :g4]]
tcs +=[n, n, n,  n]

# 16
cf += [:r]
tcf += [1]

# ojo con esta parte:
cf += [:e2, :f2, :g2, :b2, :e2, :f2, :g2, :b2]
tcf += [n, n, n,  n, n, n, n,  n]

# 17
cs += [[:g4, :bf3], :e4, :d4, :e4]
tcs +=  [c, c, c, c, 0.625]

cs += [[:b3, :e4], [:bf3, :g4], :r, [:bf3, :g4]]
tcs += [n, n, n, n]

cs += [[:g4, :bf3], :e4, :d4, :e4, :e4]
tcs +=  [c, c, c, c, b]

# 18
cf += [:c3, :bf2, :g2, :b2]
tcf +=  [n, n, n, n]

cf +=[:c3, :bf2, :g2, :c2]
tcf +=  [n, n, n, n]

cf +=[:c3, :bf2, :g2, :b2]
tcf +=  [n, n, n, n]

# 19
cs += [[:b3, :e4], :r, :r, :ds4]
tcs +=  [b, n, cp, sc]

cs +=[[:g4, :b3], :e4, :d4, :e4]
tcs += [c, c, c, c, b]

# 20
cf += [:c3, :bf2, :g2, :c2]
tcf += [n, n, n, n]

cf += [[:b2, :e2], :r, :b2, :r]
tcf += [n, n, n, n]

# 21
cs +=[[:e4, :b3], [:b3, :g4], :r]
tcs +=[n, n, b]

cs +=[[:g4, :bs3], :e4, :d4, :e4, :e4]
tcs +=[c, c, c, c, b]

# 22
cf +=[:e2, :r, :b2, [:ef2, :bf2], [:d2, :a2], [:df2, :af2]]
tcf += [n, n, cp, sc, cp, sc]

cf += [[:c2, :g2], :r, :g2, n]
tcf += [n, n, n, n]

# 23
cs +=[[:b3, :e4], [:bf3, :g4], :r]
tcs +=[n, n, n]

cs +=[[:g4, :bf3], :e4, :d4, :e4, :e4, :e4]
tcs +=[c, c, c, c, c, c]

cs +=[:r, :r, [:f5, :df5, :b4]]
tcs +=[b, n, n]

# 24
cf +=[:c2, :r, :g2, [:cs2, :es4], [:d2, :a2], [:ds2, :as2]]
tcf +=[n, n, cp, sc, cp, sc]

cf += [[:e2, :b2], :r, :b2, n]
tcf +=[n, n, n, n]

cf +=[:e2, :b2, :e3, [:e2, :b2]]
tcf += [n, n, n, n]

#####################################
use_synth :piano

with_fx :reverb, room: 0.75 do
  in_thread do
    for j in 0..cs.length-1
      play cs[j]
      sleep tcs[j]
    end
  end
  in_thread do
    for j in 0..cf.length-1
      play cf[j]
      sleep tcf[j]
    end
  end
  
  ##| in_thread do
  ##|   # plarray(:c4, cf, tcf)
  ##|   # plarray([muestras, "p_c4"], :c4, cf, tcf)
  # in_thread {plarray([muestras, "p_c4"], :c4, cf, tcf, 0.3)}
  
  ##| end
  ##| in_thread do
  ##|   # plarray([muestras0, "c4"], :c4, cs, tcs)
  ##|   # plarray(:c4, cs, tcs)
  # in_thread {plarray([muestras0, "c4"], :c4, cs, tcs, 0.5)}
  ##| end
end

#####################################


# with_fx :reverb, room: 0.75 do
##| in_thread do
##|   plarray(:c4, cf, tcf)
##| end
##| in_thread do
##|   plarray(:c4, cs, tcs)
##| end
# end
