# Welcome to Sonic Pi v3.1
use_synth :piano
use_bpm 30

play_pattern_timed [:c4, :d4, :e4, :c4], [0.25, 0.25, 0.25, 0.25], release: 0.1
play_pattern_timed [:c4, :d4, :e4, :c4], [0.25, 0.25, 0.25, 0.25], release: 0.1
play_pattern_timed [:e4, :f4, :g4], [0.25, 0.25, 0.5], release: 0.1
play_pattern_timed [:e4, :f4, :g4], [0.25, 0.25, 0.5], release: 0.1

play_pattern_timed [:g4, :a4, :g4, :f4, :e4, :c4], [0.125, 0.125, 0.125, 0.125, 0.25, 0.25], release: 0.1
play_pattern_timed [:g4, :a4, :g4, :f4, :e4, :c4], [0.125, 0.125, 0.125, 0.125, 0.25, 0.25], release: 0.1

play_pattern_timed [:d4, :g4, :c4], [0.25, 0.25, 0.5], release: 0.1
play_pattern_timed [:d4, :g4, :c4], [0.25, 0.25, 0.5], release: 0.1
