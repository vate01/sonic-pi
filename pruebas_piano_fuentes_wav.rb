# Welcome to Sonic Pi v3.1
# use_synth :piano
# use_bpm 30


muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Grand Piano"
##| sample [muestras, "f-c4"], rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1
##| sample [muestras, "f-a4"]#, rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1

define :pl do |inst,samplepitch,nv,dv,vol=1,damp=1|
  shift=note(nv)-note(samplepitch)
  if damp==1 then
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: 0.2*dv,amp: vol
  else
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: dv,amp: vol #no damping
  end
end

# pl([muestras, "f-c4"],:c4, :a4, 1, 1)

# plarray(:c4, :a4, [:c4, :d4, :e4, :c4], [1, 1, 1, 1])

#put the sample names and pitches into an array i
# i=[[inst0,samplepitch0],[inst1,samplepitch1],[inst2,samplepitch2]]

define :plarray do |samplepitch,narray,darray,vol=1,shift=0|
  narray.zip(darray) do |nv,dv|
    if nv != :r
      pl([muestras, "f-c4"],samplepitch,note(nv)+shift,dv,vol)
    end
    sleep dv
  end
end


##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.1, 1, 1, 1])
##| sleep 1
##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.25, 0.25, 0.25, 0.25])



plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])
plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])

plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])
plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])

plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1])
plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1])

