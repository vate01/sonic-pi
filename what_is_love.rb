use_bpm 124 # 124

r=4
b=2
n=1
np=1.5
c=0.5
cp=0.75

muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Grand Piano"
##| sample [muestras, "f-c4"], rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1
##| sample [muestras, "f-a4"]#, rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1

define :pl do |inst,samplepitch,nv,dv,vol=1,damp=1|
  shift=note(nv)-note(samplepitch)
  if damp==1 then
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: 0.2*dv,amp: vol
  else
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: dv,amp: vol #no damping
  end
end

##| pl([muestras, "f-c4"],:c4, :a4, 1, 1)
##| pl([muestras, "f-c4"],:c4, :b4, 1, 1)

define :acordito do |acorde, tiempo=1|
  pl([muestras, "f-c5"],:c5, acorde[0], tiempo, 1)
  pl([muestras, "f-c5"],:c5, acorde[1], tiempo, 1)
  pl([muestras, "f-c5"],:c5, acorde[2], tiempo, 1)
end

define :drums do
  sample :drum_heavy_kick, rate: 0.8, amp: 0.5
  sleep 1
  sample :drum_heavy_kick, rate: 0.8, amp: 0.5
  sample "/home/vate01/proyectos/sonic-pi/what_is_love/", "clap", amp: 0.5
  sample "/home/vate01/proyectos/sonic-pi/what_is_love/", "Shaker", amp: 0.5
  sleep 1
end

define :bass_atack do
  use_synth :dsaw
  with_octave -1 do
    use_synth_defaults release: 0.1, cutoff: 100, res: 0.8, wave: 0, amp: 0.3
    play_pattern_timed [:g4, :g4, :g4, :g4, :f,
                        :af3, :af3, :af3,:af3, :c,
                        :d, :d, :d, :d, :df,
                        :f, :f, :f, :d, :f,
    ],
      [n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c
       ]
      end
end

define :morphine do
  use_synth :piano
  use_synth_defaults amp: 15
  # with_fx :reverb do
  with_octave -1 do
    play_pattern_timed [[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :df6],
                        [:f5, :af6, :d6],[:f5, :af6, :d6],[:f5, :af6, :d6],[:f5, :af6, :d6],[:f6, :af6, :df6],
                        [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :df6],
                        [:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6]
                        
    ],
      [n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c
       ]
      end
end

define :morphine2 do
  # with_octave -1 do
  acordito([:g5, :af6, :d6])
  sleep n
  acordito([:g5, :af6, :d6])
  sleep n
  acordito([:g5, :af6, :d6])
  sleep cp
  acordito([:g5, :af6, :d6])
  sleep cp
  acordito([:g5, :af6, :df6])
  sleep c
  acordito([:f5, :af6, :d6])
  sleep n
  acordito([:f5, :af6, :d6])
  sleep n
  acordito([:f5, :af6, :d6])
  sleep cp
  acordito([:f5, :af6, :d6])
  sleep cp
  acordito([:f6, :af6, :df6])
  sleep c
  acordito([:f5, :a6, :d6])
  sleep n
  acordito([:f5, :a6, :d6])
  sleep n
  acordito([:f5, :a6, :d6])
  sleep cp
  acordito([:f5, :a6, :d6])
  sleep cp
  acordito([:f5, :a6, :df6])
  sleep c
  acordito([:a6, :c6, :f6])
  sleep n
  acordito([:a6, :c6, :f6])
  sleep n
  acordito([:a6, :c6, :f6])
  sleep cp
  acordito([:a6, :c6, :f6])
  sleep cp
  acordito([:a6, :c6, :f6])
  sleep c
  # end
end

define :lead do
  use_synth :zawa
  use_synth_defaults release: 0.2, amp: 0.1# , cutoff: 110#, res: 0.8, wave: 0
  # with_fx :gverb do
  play_pattern_timed [:af, :a, :af, :g,:af, :a, :af, :g,
                      :af, :a, :af, :f,:af, :a, :af, :f,
                      :a, :g, :a, :f,:a, :g, :a, :f,
                      :a, :g, :a, :f,:a, :g, :a, :f
  ],
    [c,c,c,c,c,c,c,c,
     c,c,c,c,c,c,c,c,
     c,c,c,c,c,c,c,c,
     c,c,c,c,c,c,c,c
     ]
    # end
    end

define :string do
  # use_synth :tb303 #
  # sleep 50
  use_synth :beep
  use_synth_defaults release: 0.5, cutoff: 110#, res: 0.8, wave: 0
  # with_fx :gverb do
  play_pattern_timed [:g6, :f6, :af7, :f6, :d6, :f6, :g6, :f6, :af7, :f6, :a7
  ],
    [r, n, np, np,
     r, r, r,
     n, np, np,
     2*r
     ]
    # end
    end


in_thread do
  10.times do
    bass_atack
  end
end

in_thread do
  4.times do
    lead
  end
end

in_thread do
  sleep 32
  62.times do
    drums
  end
end

##| in_thread do
##|   sleep 48
##|   puts "morphine"
##|   2.times do
##|     morphine
##|   end
##| end

with_fx :reverb, room: 0.9 do
  in_thread do
    sleep 48
    puts "morphine"
    2.times do
      morphine2
    end
  end
end

in_thread do
  sleep 80
  puts "volviendo lead"
  2.times do
    lead
  end
end

##| in_thread do
##|   sleep 112
##|   puts "morphine"
##|   1.times do
##|     morphine
##|   end
##| end

with_fx :reverb, room: 0.9 do
  in_thread do
    sleep 112
    puts "morphine"
    2.times do
      morphine2
    end
  end
end

in_thread do
  sleep 128
  puts "volviendo lead"
  2.times do
    lead
  end
end

in_thread do
  sleep 128
  1.times do
    string
  end
end




