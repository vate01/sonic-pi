use_bpm 124 # 124

r=4
b=2
n=1
np=1.5
c=0.5
cp=0.75

use_midi_defaults port:"midi_through_midi_through_port-0_14_0",
  vel_f: 0.7,channel:1
#enable audio input from qsynth with some reveb applied
# with_fx :reverb, room: 0.5,mix: 0.7 do
live_audio :qsynth,stereo: true

##| use_synth :dsaw
##| use_synth_defaults release: 0.1, cutoff: 100, res: 0.8, wave: 0, amp: 0.3
##| play_pattern_timed [:g4, :g4, :g4, :g4, :f,
##|                     :af3, :af3, :af3,:af3, :c,
##|                     :d, :d, :d, :d, :df,
##|                     :f, :f, :f, :d, :f,
##| ],
##|   [n, n, cp, cp, c,
##|    n, n, cp, cp, c,
##|    n, n, cp, cp, c,
##|    n, n, cp, cp, c
##|    ]


## on choisit la banque 8 pour le canal midi 1
## we select bank number 8 on the midi canal 1
##| midi_cc 0, 0, channel: 1 # canal midi
##| midi_pc 127

##| # sleep 1

##| midi_cc 0, 0, channel: 1 # canal midi
##| midi_cc 32, 8, channel: 1 # banque 8
##| midi_pc 19

# sleep 1


# midi_pc 14
# midi 60, sustain: 3

# sleep 2

define :midi_chord do |notes, *args|
  notes.each do |note|
    midi note, *args
  end
end

define :plarray do |samplepitch,narray,darray,vol=1,shift=0|
  narray.zip(darray) do |nv,dv|
    if nv != :r
      pl([muestras, "f-c4"],samplepitch,note(nv)+shift,dv,vol)
    end
    sleep dv
  end
end

define :play_my_melody do |my_note_list, my_sleep_list|
  # tick_reset(:my_melody_tick)
  my_length = my_note_list.length
  
  my_length.times do
    my_counter = tick(:my_melody_tick)
    midi my_note_list.ring[my_counter]
    sleep my_sleep_list.ring[my_counter]
  end
end

define :play_my_melody2 do |my_note_list, my_sleep_list, sustains|
  # tick_reset(:my_melody_tick)
  my_length = my_note_list.length
  
  my_length.times do
    my_counter = tick(:my_melody_tick)
    midi my_note_list.ring[my_counter], sustain: sustains[my_counter]
    sleep my_sleep_list.ring[my_counter]
  end
end



# midi_pc 39

##| with_fx :reverb do
##|   with_octave -1 do
##|     play_my_melody([:g4, :g4, :g4, :g4, :f,
##|                     :af3, :af3, :af3,:af3, :c,
##|                     :d, :d, :d, :d, :df,
##|                     :f, :f, :f, :d, :f,
##|                    ], [n, n, cp, cp, c,
##|                        n, n, cp, cp, c,
##|                        n, n, cp, cp, c,
##|                        n, n, cp, cp, c
##|                        ])
##|   end
##| end

# sleep 2

##| use_synth :dsaw
##| with_octave -1 do
##|   use_synth_defaults release: 0.1, cutoff: 100, res: 0.8, wave: 0, amp: 0.3
##|   play_pattern_timed [:g4, :g4, :g4, :g4, :f,
##|                       :af3, :af3, :af3,:af3, :c,
##|                       :d, :d, :d, :d, :df,
##|                       :f, :f, :f, :d, :f,
##|   ],
##|     [n, n, cp, cp, c,
##|      n, n, cp, cp, c,
##|      n, n, cp, cp, c,
##|      n, n, cp, cp, c
##|      ]
##|     end

##| sleep 2

##| use_synth :zawa
##| use_synth_defaults release: 0.2, amp: 0.1# , cutoff: 110#, res: 0.8, wave: 0
##| # with_fx :gverb do
##| play_pattern_timed [:af, :a, :af, :g,:af, :a, :af, :g,
##|                     :af, :a, :af, :f,:af, :a, :af, :f,
##|                     :a, :g, :a, :f,:a, :g, :a, :f,
##|                     :a, :g, :a, :f,:a, :g, :a, :f
##| ],
##|   [c,c,c,c,c,c,c,c,
##|    c,c,c,c,c,c,c,c,
##|    c,c,c,c,c,c,c,c,
##|    c,c,c,c,c,c,c,c
##|    ]

# sleep 2

##| midi_cc 0, 0, channel: 1 # canal midi
##| # midi_cc 32, 8 # banque 8
##| midi_pc 56
##| # midi 60

define :lead2 do
  play_my_melody2( [:af, :a, :af, :g,:af, :a, :af, :g,
                    :af, :a, :af, :f,:af, :a, :af, :f,
                    :a, :g, :a, :f,:a, :g, :a, :f,
                    :a, :g, :a, :f,:a, :g, :a, :f
                    ],
                   [c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c
                    ],
                   [c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c,
                    c,c,c,c,c,c,c,c
                    ])
end

define :bass_atack2 do
  with_fx :reverb do
    with_octave -1 do
      play_my_melody([:g4, :g4, :g4, :g4, :f,
                      :af3, :af3, :af3,:af3, :c,
                      :d, :d, :d, :d, :df,
                      :f, :f, :f, :d, :f,
                     ], [n, n, cp, cp, c,
                         n, n, cp, cp, c,
                         n, n, cp, cp, c,
                         n, n, cp, cp, c
                         ])
    end
  end
end


define :morphine do
  use_synth :piano
  use_synth_defaults amp: 1
  # with_fx :reverb do
  with_octave -1 do
    play_pattern_timed [[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :d6],[:g5, :af6, :df6],
                        [:f5, :af6, :d6],[:f5, :af6, :d6],[:f5, :af6, :d6],[:f5, :af6, :d6],[:f6, :af6, :df6],
                        [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :d6], [:f5, :a6, :df6],
                        [:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6],[:a6, :c6, :f6]
                        
    ],
      [n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c,
       n, n, cp, cp, c
       ]
      end
end


midi_cc 0, 0, channel: 1 # canal midi
midi_pc 56
lead2

# sleep 1.5

midi_cc 32, 8, channel: 1 # banque 8
midi_pc 39
bass_atack2

morphine

midi_all_notes_off



