"/Users/vate01/Downloads/Level Up Taincel/14 SUB DROP b.wav"

use_bpm 16 # 128

r=1
b=0.5
bp=0.75
n=0.25
np=0.375
c=0.125
cp=0.1875
sc=0.0625
scp=0.09375
f=0.03125
sf=0.015625

use_synth :piano

define :pianito do
  play_pattern_timed [[:a5, :e5, :a4],:r, [:a5, :e5, :a4], :r, [:a5, :e5, :a4],:r, [:a5, :e5, :a4], :r, [:a5, :e5, :a4],
                      [:b5, :e5, :b4, :c4],:r, [:b5, :e5, :b4, :c4], :r, [:b5, :e5, :b4, :c4], :r, [:b5, :e5, :b4, :c4], :r, [:b5, :e5, :b4, :c4],
                      [:a5, :fb5, :cb5, :cb4],:r, [:a5, :fb5, :cb5, :cb4],:r, [:a5, :fb5, :cb5, :cb4],:r, [:a5, :fb5, :cb5, :cb4],:r, [:a5, :fb5, :cb5, :cb4],
  [:cb5, :d5, :a4, :d4],:r, [:cb5, :d5, :a4, :d4],:r, [:cb5, :d5, :a4, :d4],:r, [:b5, :d5, :a4, :d4],:r, [:a5, :d5, :a4, :d4]],
    [sc, f, sc, f, sc, sc, sc, sc, sc,
     sc, f, sc, f, sc, sc, sc, sc, sc,
     sc, f, sc, f, sc, sc, sc, sc, sc,
     sc, f, sc, f, sc, sc, sc, sc, sc]
    end

define :pianito_efecto do
  use_synth :piano
  play_pattern_timed [[:a5, :e5, :a4], :r, [:a5, :e5, :a4], :r, [:a5, :e5, :a4], :r, [:a5, :e5, :a4], :r, [:a5, :e5, :a4],
                      [:b5, :e5, :b4, :c4],:r, [:b5, :e5, :b4, :c4],:r, [:b5, :e5, :b4, :c4],:r, [:b5, :e5, :b4, :c4], :r, [:b5, :e5, :b4, :c4],
  [:a5, :fb5, :cb5, :cb4], :r, [:a5, :fb5, :cb5, :cb4], :r, [:a5, :fb5, :cb5, :cb4], :r, [:a5, :fb5, :cb5, :cb4],:r, [:a5, :fb5, :cb5, :cb4]],
    [sc, f, sc, f, sc, sc, sc, sc, sc,
     sc, f, sc, f, sc, sc, sc, sc, sc,
     sc, f, sc, f, sc, sc, sc, sc, sc]
    
  # sample :perc_bell
  sample :elec_filt_snare, rate: -1, beat_stretch: b-0.1, amp: 2
  
  play_pattern_timed [[:cb5, :d5, :a4, :d4],:r, [:cb5, :d5, :a4, :d4],:r, [:cb5, :d5, :a4, :d4],:r, [:b5, :d5, :a4, :d4],:r, [:a5, :d5, :a4, :d4]],
    [sc, f, sc, f, sc, sc, sc, sc, sc]
end

define :bajito do
  use_synth_defaults amp: 0.5
  use_synth :fm
  play_pattern_timed [:a2, :e2, :fb2, :d2], [b, b, b, b]
end

define :clapyplatillitos do
  sample :sn_dub, amp: 0.1
  sample :drum_splash_soft, amp: 0.1
  sleep c
end

define :clap do
  # sample "/home/vate01/Descargas/Level Up Taincel/", "Clap"
  sample :sn_dub, amp: 0.1
  sample :drum_splash_soft, amp: 0.1
  sleep c
end

define :bd_house_b do
  sample :bd_haus, amp: 1
  sleep b # sc+ f+ sc+ f+ sc+ sc+ sc+ sc+ sc
end

define :spf do
  sample :drum_snare_soft, amp: 0.1, beat_stretch: sf
  sample :drum_cymbal_pedal, amp: 0.1, beat_stretch: sf
  sample :drum_cymbal_closed, amp: 0.1, beat_stretch: sf
  sleep f
end

define :spsf do
  sample :drum_snare_soft, amp: 0.1, beat_stretch: sf
  sample :drum_cymbal_pedal, amp: 0.1, beat_stretch: sf
  sample :drum_cymbal_closed, amp: 0.1, beat_stretch: sf
  sleep sf
end

define :melodia_dos do
  play_pattern_timed [:e6, :a5, :e6, :a5, :e6, :a5, :e6,
                      :e6,:e6, :gs5, :e6, :gs5, :e6, :gs5,
                      :e6, :e6,:fs6, :a5, :fs6, :a5, :e6, :a5,
                      :ds6, :a5, :ds6, :d6, :a5, :d6, :a5,
  :e6, :a5, :fs6, :a5, :e6],
    [sc, sc, sc, f, sc, f, sc+sc, sc+sc, sc, sc, f, sc, f, sc+sc, sc,
     sc, sc, sc, f, sc, f, sc, sc, sc,     sc, sc, sc, sc, sc, sc, sc, sc]
    end

sample :elec_filt_snare, beat_stretch: 4*b, amp: 1
sample :elec_filt_snare, amp: 1

1.times do
  pianito_efecto
end

sample :elec_filt_snare, beat_stretch: 4*b, amp: 1
sample :elec_filt_snare, beat_stretch: 1*b, amp: 1

in_thread do
  loop do
    bajito
  end
end

in_thread do
  loop do
    pianito
  end
end

in_thread do
  loop do
    clap
  end
end

in_thread do
  loop do
    bd_house_b
  end
end

in_thread do
  loop do
    spf
  end
end

in_thread do
  sleep 2
  sample :elec_filt_snare, beat_stretch: r, amp: 0.5
  16.times do
    sample :sn_dub, amp: 0.5
    sleep c
  end
end

in_thread do
  sleep 4
  24.times do
    sample :sn_dub, amp: 0.5
    sleep sc
  end
  16.times do
    sample :sn_dub, amp: 0.5
    sleep f
  end
end

# in_thread do
#  sleep 6
#  sample "/Users/vate01/Downloads/Level Up Taincel/14 SUB DROP b.wav"
# end

in_thread do
  sleep 2
  loop do
    use_synth_defaults amp: 0.1
    use_synth :saw
    melodia_dos
  end
end


# in_thread do
#   128.times do
#     spsf
#   end
# end

