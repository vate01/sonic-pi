

# use_synth :prophet


# primera línea:
play_pattern_timed [:r, :r, :r, :d], [0.5, 0.25, 0.25, 0.125]

play_pattern_timed [:e, :r, :r, :f, :g, :r, :r, :r, :ds], [0.125, 0.125, 0.25, 0.125, 0.125, 0.125, 0.125, 0.25, 0.125]

play_pattern_timed [:e, :r, :f, :g, :r, :c5, :b, :r, :r, :e, :g, :r, :b],
  [0.125, 0.125, 0.125, 0.125, 0.125, 0.0625, 0.0625, 0.125, 0.125, 0.125, 0.125, 0.125]
# [0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.0625, 0.0625, 0.125, 0.125, 0.125, 0.125]

# segunda línea:
play_pattern_timed [:eb, :e, :a, :r, :g, :r, :e, :d, :e], [0.5, 0.125, 0.0625, 0.0625, 0.0625, 0.0625, 0.125, 0.125, 0.125]

play_pattern_timed [:e, :r, :r, :ds], [0.5, 0.25, 0.25, 0.125]

play_pattern_timed [:e, :r, :r, :r, :f, :g, :r, :r, :r, :ds], [0.125, 0.125, 0.125, 0.25, 0.125, 0.125, 0.125, 0.125, 0.25, 0.125]

# tercera línea:
play_pattern_timed [:e, :r, :f, :g, :r, :c5, :b, :r, :g, :b, :r, :b5], [0.125]

play_pattern_timed [:r],[1]

play_pattern_timed [:e5, :r, :r, :r, :r, :r, :ds], [0.0625, 0.0625, 0.25, 0.25, 0.25, 0.25, 0.125]

# cuarta línea:
play_pattern_timed [:e, :r, :r, :r, :f, :g, :r, :r, :r, :ds], [0.125, 0.125, 0.125, 0.25, 0.125, 0.125, 0.125, 0.125, 0.25, 0.125]

play_pattern_timed [:e, :r, :f, :r, :g, :r, :c5, :b, :r, :e, :g, :r, :b],[0.125, 0.125, 0.0625, 0.0625, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125]

#  voy a la quinta línea de
# http://easymusic.altervista.org/the-pink-panther-violin-tab-sheet-music-guitar-chords/

play_pattern_timed [:eb, :e, :r, :a, :g, :r, :e, :d, :e], [0.5, 0.0625, 0.0625, 0.125, 0.0625, 0.0625, 0.125, 0.125, 0.125]

play_pattern_timed [:r], [1]

play_pattern_timed [:r, :e5, :d5, :r, :b, :a, :g, :e, :r], [0.25, 0.25, 0.0625, 0.0625, 0.25, 0.125, 0.25, 0.0625, 0.0625]

# sexta línea:
play_pattern_timed [:bb, :a, :r,   :b, :a, :r,    :b, :a, :r,   :b, :a], [0.25, 0.0625, 0.0625,  0.25, 0.0625, 0.0625, 0.25, 0.0625, 0.0625, 0.25, 0.125]
play_pattern_timed [:g, :r, :e, :r, :d, :e, :r, :e, :e, :r, :r, :r, :ds],
  [0.0625, 0.0625, 0.0625, 0.125, 0.125, 0.0625, 0.125, 0.5, 0.5, 0.5, 0.25, 0.25, 0.125]


# séptima línea:
play_pattern_timed [:g, :r, :e, :r, :d, :e, :r, :e, :e, :r],
  [0.0625, 0.0625, 0.0625, 0.0625, 0.125,    0.125, 0.0625, 0.125, 0.125]

play_pattern_timed [:g, :r, :e, :r, :d, :e, :r, :e, :e, :r],   [0.0625, 0.0625, 0.0625, 0.0625, 0.125,    0.125, 0.0625, 0.125, 0.125]


# octava:
play_pattern_timed [:g, :r, :e, :r, :d, :e, :r, :e, :e, :r],
  [0.0625, 0.0625, 0.0625, 0.0625, 0.125,    0.125, 0.0625, 0.125, 0.125]

play_pattern_timed [:e, :b, :e5, :r, :r], [0.25, 0.25, 0.25, 0.25, 1]

