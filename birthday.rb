# Welcome to Sonic Pi
# use_synth :piano
# use_synth :fm
use_bpm 180

play_pattern_timed [:r, :g, :g], [2,0.5,0.5]
play_pattern_timed [:a, :g, :c5], [1,1,1]
play_pattern_timed [:b, :g, :g], [2,0.5,0.5]
play_pattern_timed [:a, :g, :d5], [1,1,1]

play_pattern_timed [:c5, :c5, :e5], [2,0.5,0.5]
play_pattern_timed [:g5, :c5, :c5], [1,1,1]
play_pattern_timed [:b, :a, :f5, :f5], [1,1,0.5, 0.5]
play_pattern_timed [:e5, :c5, :d5], [1,1,1]

