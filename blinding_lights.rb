use_bpm 85

r=4
b=2
n=1
np=1.5
c=0.5
sc=0.25
cp=0.75

midi_all_notes_off

live_audio :qsynth,stereo: true

define :play_my_melody4 do |my_note_list, my_sleep_list, sustains, canal|
  my_length = my_note_list.length
  my_length.times do
    my_counter = tick(:my_melody_tick)
    midi_note_on my_note_list.ring[my_counter], channel: canal, sustain: sustains[my_counter], velocity: 50
    sleep my_sleep_list.ring[my_counter]
    midi_note_off my_note_list.ring[my_counter], channel: canal
    # midi_all_notes_off
  end
end

# midi_cc 32, 8, channel: 1
midi_pc 39, channel: 1
midi_pc 22, channel: 4
midi_pc 87, channel: 5


define :lead3 do
  play_my_melody4( [:f0, :e1, :f1, :c1, :e1, :f1, :b0, :b0, :a0, :g0,
                    :f0, :e1, :f1, :c1, :e1, :f1, :b0, :b0, :a0, :g0
                    # :f1, :e2, :f2, :c2, :e2, :f2, :b1, :b1, :a1, :g1
                    ],
                   [
                     1.5,0.25,0.25,2,1.5,0.25,1.25,0.375,0.375,0.25, # 8 en total
                     1.5,0.25,0.25,2,1.5,0.25,1.25,0.375,0.375,0.25
                   ],
                   [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
                    ], 1)
end

define :melody3 do
  play_my_melody4( [:f5, :f5, :eb5, # 1
                    :f5, :g5, :c5, :eb5, # 2
                    :f5, :f5, :eb5, # 3
                    :f5, :g5, :c5, :eb5, # 4
                    :bb6, :g5, :f5, :eb5, # 5
                    :bb6, :g5, :f5, :eb5, :f5, # 6
                    :r
                    ],
                   [
                     1,0.75,0.25,
                     0.25,0.5,0.5,0.75,
                     1,0.75,0.25,
                     0.25,0.5,0.5,0.75,
                     0.25,0.5,0.5,0.75,
                     0.25,0.5,0.5,2.75,
                     2
                   ],
                   [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
                    ], 5)
end


define :melody4 do
  play_my_melody4( [:f5, :f5, :eb5, # 1
                    :f5, :g5, :c5, :eb5, # 2
                    :f5, :f5, :eb5, # 3
                    :f5, :g5, :c5, :eb5, # 4
                    :bb6, :g5, :f5, :eb5, # 5
                    :bb6, :g5, :f5, :eb5, :f5, # 6
                    :r
                    ],
                   [
                     1,0.75,0.25,
                     0.25,0.5,0.5,0.75,
                     1,0.75,0.25,
                     0.25,0.5,0.5,0.75,
                     0.25,0.5,0.5,0.75,
                     0.25,0.5,0.5,2.75,
                     2
                   ],
                   [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
                    0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
                    ], 4)
end

sleep 3
##| in_thread do
##|   loop do
##|     melody4
##|   end
##| end

##| in_thread do
##|   loop do
##|     cue :tick
##|     lead3
##|   end
##| end

##| in_thread do
##|   loop do
##|     sync :tick
##|     melody3
##|   end
##| end

##| sleep 200

# lead3
in_thread do
  loop do
    lead3
  end
end

in_thread do
  sleep 16
  3.times do
    melody3
  end
end


in_thread do
  sleep 80
  2.times do
    melody4
  end
end

# https://gistlog.co/manualbashing/f7b91ecb9ad2169edd7844d7c7f0c07c
kick = [1, 0, 0, 0,  1, 0, 0, 0,  1, 0, 0, 0,  1, 1, 0, 0]
clap = [0, 0, 1, 0,  0, 0, 1, 0,  0, 0, 1, 0,  0, 0, 1, 0]
plat = [1, 1, 0, 1,  1, 1, 0, 1,  1, 1, 0, 1,  1, 1, 0, 1]
aplausos = [0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 0, 1, 1]

sleep 32
4.times do
  16.times do |i|
    sample :bd_haus, amp: 0.1 if kick[i] == 1
    sleep 0.25
  end
end

4.times do
  16.times do |i|
    sample :bd_haus, amp: 0.1 if kick[i] == 1
    sample :sn_dolf, amp: 0.1 if clap[i] == 1
    sleep 0.25
  end
end

in_thread do
  loop do
    16.times do |i|
      sample :bd_haus, amp: 0.1 if kick[i] == 1
      sample :sn_dolf, amp: 0.1 if clap[i] == 1
      sample :drum_cymbal_soft, amp: 0.01, rate: 0.8 if plat[i] == 1
      sample "/home/vate01/proyectos/sonic-pi/what_is_love/", "clap", amp: 0.01 if aplausos[i] == 1
      sleep 0.25
    end
  end
end

##| in_thread do
##|   melody3
##| end

