# Welcome to Sonic Pi v3.1
# use_synth :piano
use_bpm 47
# repositorio de donde tomé las funciones
# https://gist.github.com/rbnpi/01008ecad37ecb18e9a0
# o
# https://gist.github.com/nppoly/02aa3169029a114891ac3d121bb8650e
# ideas en:
# https://in-thread.sonic-pi.net/t/adding-an-orchestra-to-sonic-pi-using-samples/141
# y más complejo en
# https://rbnrpi.wordpress.com/2016/03/16/sonatina-symphonic-orchestra-revisited-to-give-55-sample-voices-for-sonic-pi/

#  muestras = "/home/vate01/Descargas/sonic_pi_related/sso-master/Sonatina Symphonic Orchestra/Samples/Grand Piano"
# muestras = "/Users/vate01/Desktop/pi/GrandPiano/GrandPiano/"
##| sample [muestras, "f-c4"], rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1
##| sample [muestras, "f-a4"]#, rate: (pitch_to_ratio note(:a4) - note(:c4))
##| sleep 1


define :pl do |inst,samplepitch,nv,dv,vol=1,damp=1|
  shift=note(nv)-note(samplepitch)
  if damp==1 then
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: 0.2*dv,amp: vol
  else
    sample inst,rate: (pitch_to_ratio shift),sustain: 0.8*dv,release: dv,amp: vol #no damping
  end
end

define :plarray do |samplepitch,narray,darray,vol=1,shift=0|
  narray.zip(darray) do |nv,dv|
    if nv != :r
      pl([muestras, "f_c4"], samplepitch,note(nv)+shift,dv,vol)
    end
    sleep dv
  end
end

# pl([muestras, "f_c4"],:c4, :a4, 1, 1)

# plarray(:c4, :a4, [:c4, :d4, :e4, :c4], [1, 1, 1, 1])

#put the sample names and pitches into an array i
# i=[[inst0,samplepitch0],[inst1,samplepitch1],[inst2,samplepitch2]]




##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.1, 1, 1, 1])
##| sleep 1
##| plarray(:c4, [:c4, :d4, :e4, :c4], [0.25, 0.25, 0.25, 0.25])

# plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
# plarray(:c4, [:c4, :d4, :e4, :c4], [0.5, 0.5, 0.5, 0.5])
# plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])
# plarray(:c4, [:e4, :f4, :g4], [0.5, 0.5, 1])

# plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])
# plarray(:c4, [:g4, :a4, :g4, :f4, :e4, :c4], [0.25, 0.25, 0.25, 0.25, 0.5, 0.5])

# plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1])
# plarray(:c4, [:d4, :g4, :c4], [0.5, 0.5, 1]


r=1
b=0.5
bp=0.75
n=0.25
np=0.375
c=0.125
cp=0.1875
sc=0.0625
scp=0.09375
f=0.03125
sf=0.015625

# 1
cs=[:d5, :d5, :f5, :e5]
tcs=[b, c, np, r]

cs+=[:bb4, :c5, :bb4, :bb4, :a4]
tcs+=[c, n, c, c, np]

cs +=[:f4, :g4, :f4, :f4, :e4]
tcs +=[c, n, c, c, np]

cs +=[:d5, :d5, :f5, :ef5]
tcs +=[b, c, np, r]

# 2
cf=[[:g2, :g3, :b3, :d4], [:c2, :g3, :a3, :c4], [:a2, :e3, :a3, :c4],
    [:e2, :e3, :g3, :b3], [:g2, :g3, :b3, :d4], [:c2, :g3, :a3, :c4]]
tcf=[r, r, r, r, r, r]


# 3
cs+=[:bb4, :c5, :bb4, :bb4, :ab4]
tcs+=[c, n, c, c, cp]

cs+=[:f4, :g4, :f4, :f4, :e4]
tcs+=[c, n, c, c, np]

cs +=[[:g4, :bb4, :d5], [:g4, :bb4, :d5], [:g4, :f5]]
tcs+=[b, c, np]

# 4
cf +=[[:a2, :eb3, :a3, :c4], [:e2, :eb3, :g3, :b3], :g2, :g2, :g2, :g2, :g2, :g2, :g2, :g2]
tcf +=[r, r, c,c,c,c, c,c,c,c]

# 5
cs+=[[:g4, :c5, :e5]]
tcs+=[r]

cs+=[[:e4, :b4], [:e4, :c5], [:e4, :bb4], [:e4, :bb4], [:e4, :a4]]
tcs+=[c, n, c, c, np]

# 6
cf+=[:c2, :c2, :c2, :c2, :c2, :c2, :c2, :c2]
tcf +=[c, c, c, c, c, c, c, c]

cf+=[:a2, :a2, :a2, :a2, :a2, :a2, :a2, :a2]
tcf +=[c, c, c, c, c, c, c, c]

# 7
cs+=[[:b3, :f4], [:b3, :g4], [:b3, :f4], [:b3, :f4], [:b3, :e4]]
tcs+=[c, n, c, c, n]

cs+=[[:g4, :bb4, :d5], [:g4, :bb4, :d5], [:g4, :f5]]
tcs+=[b, c, np]

# 8
cf+=[:e2, :e2, :e2, :e2, :e2, :e2, :e2, :e2]
tcf +=[c, c, c, c, c, c, c, c]

cf+=[:g2, :g2, :g2, :g2, :g2, :g2, :g2, :g2]
tcf +=[c, c, c, c, c, c, c, c]

# 9
cs+=[[:g4, :c5, :eb5], [:e4, :b4], [:e4, :c5], [:e4, :b4], [:e4, :b4], [:e4, :ab4]]
tcs+=[r, c, n, c, c, np]

# 10
cf+=[:cb2, :cb2, :cb2, :cb2, :cb2, :cb2, :cb2, :cb2]
tcf +=[c, c, c, c, c, c, c, c]

cf+=[:a2, :a2, :a2, :a2, :a2, :a2, :a2, :a2]
tcf +=[c, c, c, c, c, c, c, c]

# 11
cs+=[[:b3, :f4], [:b3, :g4], [:b3, :f4], [:b3, :f4], [:b3, :e4]]
tcs+=[c, n, c, c, np]

cs+=[[:e4, :g4, :c5], [:e4, :g4, :c5], [:e4, :bb4], [:d4, :g4, :bb4]]
tcf +=[r, c, cp, r]

# 12
cf+=[:e2, :e2, :e2, :e2, :e2, :e2, :e2, :e2]
tcf +=[c, c, c, c, c, c, c, c]

cf+=[:c3, :c3, :c3, :c3, :c3, :g2, :g2, :g2, :g2, :g2]
tcf +=[np, c, c, n, c, np, c, c, n, c]


#####################################

in_thread do
  loop do
    cue :tick
    sleep 1
  end
end

in_thread do
  use_synth :piano
  play_pattern_timed cs, tcs
  sleep 1
end

in_thread do
  use_synth :fm
  play_pattern_timed cf, tcf, amp: 0.05
  sleep 1
end

##| in_thread do
##|   loop do
##|     sync :tick
##|     sample :drum_heavy_kick, amp: 0.2
##|   end
##| end
#####################################


