use_bpm 60

r=4
b=2
n=1
np=1.5
c=0.5
sc=0.25
cp=0.75

midi_all_notes_off

live_audio :qsynth,stereo: true

# http://www.keeper1st.com/music/animsolo.pdf
# https://in-thread.sonic-pi.net/t/how-to-send-midi-chords-through-vsti-via-sonic-pi/812/12
define :midi_chord do |notes, canal, release|
  notes.each do |note|
    midi note, channel: canal, release: release
  end
end

midi_pc 0, channel: 1

##| midi_chord([:g, :b, :e], 1, 0.1)
##| sleep 1
##| midi_chord([:g2, :g3], 1, 2)

live_loop :midich do
  midi_chord chord(:c4,:major), 1, 6
  sleep 4
end




