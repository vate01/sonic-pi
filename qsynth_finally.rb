#midi test program
#set midi port, channel and velocity factor
use_midi_defaults port:"midi_through_midi_through_port-0_14_0", vel_f: 0.7,channel:2
#enable audio input from qsynth with some reveb applied
# with_fx :reverb, room: 0.5,mix: 0.7 do
live_audio :qsynth,stereo: true
# end
#select a different instrument every 32 notes
##| live_loop :selectInstrument do
##|   n=tick%118
##|   midi_pc n
##|   sleep 3.2
##| end

##| #select random notes from the pentatonic minor scale to play
##| live_loop :playmidi do
##|   midi scale(:c2,:minor_pentatonic,num_octaves:4).choose,sustain: 0.1
##|   sleep 0.1
##| end


midi_pc 42
midi 60, sustain: 3

sleep 1

midi_pc 12
midi 60

sleep 1

define :pattern do
  midi :e1, release: 0.2
  sleep 0.1
  midi :g1, release: 0.2
  sleep 0.1
  midi :e1, release: 0.5
end

pattern

sleep 1

# https://in-thread.sonic-pi.net/t/how-to-send-midi-chords-through-vsti-via-sonic-pi/812/8
(midi_notes :d3, :d4, :d5).each do |n|
  midi n, release: 0.1
  sleep 0.1
end

sleep 1

(chord :a2, :major).each do |n|
  midi n, release: 0.1
  sleep 0.1
end

sleep 1

midi_pc 22

(chord :f2, :major).each do |n|
  midi n, release: 0.1
  sleep 0.1
end

define :midi_chord do |notes, *args|
  notes.each do |note|
    midi note, *args
  end
end

sleep 1

## on choisit la banque 8 pour le canal midi 1
## we select bank number 8 on the midi canal 1
midi_cc 0, 0, channel: 2 # canal midi
# midi_cc 32, 8 # banque 8
midi_pc 127
midi 60, sustain: 3

sleep 1

midi_cc 0, 0, channel: 1 # canal midi
# midi_cc 32, 8 # banque 8
midi_pc 42
midi 60, sustain: 3
